
from chaotic import chaotic
from cp_one import join, meet, bottom, top, iota

succ = {1:{2}, 2:{3,4}, 3:{5}, 4:{5}, 5:{}} # CFG edges
tr = {  # transfer function
    (1, 2): lambda x: 3 if x != bottom else bottom,
    (2, 3): lambda x: x,
    (2, 4): lambda x: meet(x, 4),
    (3, 5): lambda x: x,
    (4, 5): lambda x: 8 if x != bottom else bottom,
}
tr_txt  = {   # for debugging
    (1,2): "x := 3",
    (2, 3): "[x != 4]",
    (2, 4): "[x = 4]",
    (3, 5): "nop",
    (4, 5): "x := 8",
}

chaotic(succ, 1, iota, join, bottom, tr, tr_txt)
